<?php
include("opendb.php");
require_once ("core/functions.php");
ini_set('default_charset', 'utf-8');

$isAdmin = false;
$rank = 0;
if(isLoggedIn()){
    $sql = "SELECT rank FROM users WHERE id = ?";
    $param_id = 0;
    if($stmt = mysqli_prepare($conn, $sql)) {
        mysqli_stmt_bind_param($stmt, "i", $param_id);
        $param_id = $_SESSION["id"];
        if (mysqli_stmt_execute($stmt)) {
            mysqli_stmt_store_result($stmt);
            if (mysqli_stmt_num_rows($stmt) == 1) {
                mysqli_stmt_bind_result($stmt, $rank);
                if (mysqli_stmt_fetch($stmt)) {
                    if ($rank > 0) {
                        $isAdmin = true;
                    }
                }
            }
        }
    }
}

?>

<html>
<head>
    <link href="/bootstrap-4.3.1/css/bootstrap.css" rel="stylesheet">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="/css/main.css" rel="stylesheet">
    <title>Tienda de libros</title>
    <script src="/bootstrap-4.3.1/js/jquery-3.4.1.min.js"></script>
    <script src="/bootstrap-4.3.1/js/bootstrap.js"></script>
<head>
<body>
<nav class="navbar navbar-expand-md navbar-light mb-4" style="background-color: #e3f2fd;">
    <div class="header_menu">
        <a class="navbar-brand" href="#"> » Tienda Libros</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/">Inicio <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">Contacto</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">Ubicación</a>
                </li>
            </ul>
            <?php if($isAdmin){ ?>
                <div class="btn-group">
                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Gestión
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="?p=all_orders">Pedidos</a>
                        <a class="dropdown-item" href="?p=clients">Clientes</a>
                        <a class="dropdown-item" href="?p=new_book">Nuevo libro</a>
                    </div>
                </div>
            <?php }?>
            <div class="btn-group">
                <button type="button" style="margin-left:5px;" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php if(isLoggedIn())
                            echo "Hola, " . $_SESSION["username"];
                        else
                            echo "Iniciar sesión";
                     ?>
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                    <?php if(isLoggedIn()){ ?>
                        <a class="dropdown-item" href="#">Perfil</a>
                        <a class="dropdown-item" href="?p=history_orders">Mis Pedidos</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="?p=logout">Cerrar sesión</a>
                    <?php } else { ?>
                        <a class="dropdown-item" href="?p=login">Iniciar sesión</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="?p=register">Crear cuenta</a>
                    <?php }?>
                </div>
            </div>
        </div>
    </div>
</nav>

<main role="main" class="container">

    <?php
    error_reporting(E_ALL ^ E_NOTICE);
    $var = "p";
    $defaultname = "home";
    $default = "home.php";
    if (file_exists("pages/$_GET[$var].php"))
    {
        include ("pages/$_GET[$var].php");
    }
    else if ($_GET[$var] == "$defaultname" || !$_GET[$var])
    {
        include("pages/$default");
    }
    else
    {
        echo("<div class=\"alert alert-error alert-block\"> <a class=\"close\" data-dismiss=\"alert\" href=\"#\">×</a><h4 class=\"alert-heading\">¡Error!</h4>La página que busca no existe.</div>");
    }
    ?>

</main>
</body>
</html>