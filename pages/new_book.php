<?php
require_once("core/new_books.php");
?>
<h2>Nuevo libro</h2>
<p>Ingrese los datos necesarios.</p>
<form action="#" method="post">
    <div class="form-row">
        <div class="form-group col-md-6 <?php echo (!empty($name_err)) ? 'has-error' : ''; ?>">
            <label><b>Nombre del libro</b></label>
            <input type="text" name="name" class="form-control" value="<?php echo $name; ?>">
            <span class="help-block"><?php echo $name_err; ?></span>
        </div>

        <div class="form-group col-md-6 <?php echo (!empty($isbn_err)) ? 'has-error' : ''; ?>">
            <label><b>ISBN</b></label>
            <input type="text" name="isbn" class="form-control" value="<?php echo $isbn; ?>">
            <span class="help-block"><?php echo $isbn_err; ?></span>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group  col-md-6 <?php echo (!empty($autor_err)) ? 'has-error' : ''; ?>">
            <label><b>Autor</b></label>
            <input type="text" name="autor" class="form-control" value="<?php echo $autor; ?>">
            <span class="help-block"><?php echo $autor_err; ?></span>
        </div>

        <div class="form-group  col-md-6 <?php echo (!empty($publisher_err)) ? 'has-error' : ''; ?>">
            <label><b>Editorial</b></label>
            <input type="text" name="publisher" class="form-control" value="<?php echo $publisher; ?>">
            <span class="help-block"><?php echo $publisher_err; ?></span>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6 <?php echo (!empty($category_err)) ? 'has-error' : ''; ?>">
            <label><b>Categoría</b></label>
            <select name="category" class="form-control">
                <?php
                $getCategories = mysqli_query($conn,"SELECT * FROM `categories`");
                while ($cats = mysqli_fetch_array($getCategories)){ ?>
                    <option value="<?php echo ("$cats[id]"); ?>"><?php echo ("$cats[name]"); ?></option>
                <?php } ?>
            </select>
            <span class="help-block"><?php echo $category_err; ?></span>
        </div>

        <div class="form-group col-md-6 <?php echo (!empty($image_err)) ? 'has-error' : ''; ?>">
            <label><b>Imagen</b></label>
            <div class="custom-file">
                <input type="file" name="image" class="custom-file-input" id="image_input">
                <label class="custom-file-label" for="image_input">Seleccionar imagen</label>
            </div>

            <span class="help-block"><?php echo $image_err; ?></span>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6 <?php echo (!empty($price_err)) ? 'has-error' : ''; ?>">
            <label><b>Precio</b></label>
            <input type="text" name="price" class="form-control" value="<?php echo $price; ?>">
            <span class="help-block"><?php echo $price_err; ?></span>
        </div>

        <div class="form-group col-md-6 <?php echo (!empty($stock_err)) ? 'has-error' : ''; ?>">
            <label><b>Stock</b></label>
            <div class="form-check">
                <input class="form-check-input" name="stock" type="checkbox" id="nameCheck" value="1">
                <label class="form-check-label" for="nameCheck">
                    Sí
                </label>
            </div>
            <span class="help-block"><?php echo $stock_err; ?></span>
        </div>
    </div>
    <div class="form-group <?php echo (!empty($synopsis_err)) ? 'has-error' : ''; ?>">
        <label><b>Sinopsis</b></label>
        <textarea name="synopsis" class="form-control" value="<?php echo $synopsis; ?>"></textarea>
        <span class="help-block"><?php echo $synopsis_err; ?></span>
    </div>


    <div class="form-group">
        <input type="reset" class="btn btn-default" value="Limpiar">
        <input style="float:right;" type="submit" class="btn btn-primary" value="Agregar">
    </div>
</form>