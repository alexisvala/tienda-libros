<?php ?>
<h2><b>Pedidos</b></h2>
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Detalle</th>
        <th scope="col"></th>
        <th scope="col">Estatus</th>
    </tr>
    </thead>
    <tbody>
    <?php
    if(!empty(trim($_GET['id']))) {
        $getOrder = mysqli_query($conn, "SELECT * FROM `orders` WHERE  idUser = ". trim($_GET['id']));
    }else{
        $getOrder = mysqli_query($conn, "SELECT * FROM `orders` WHERE `status`= 2");
    }
    if(mysqli_num_rows($getOrder) != 0){
    while ($order = mysqli_fetch_array($getOrder)) { ?>
        <tr>
        <th scope="row">Order #<?php echo $order[id];?></th>
        <td colspan="2">Precio: <?php echo number_format($order[finalPrice], 2, '.', '');?> </td>
            <?php if($order[status] == 1){ ?>
                <td>Editando.</td>
            <?php }else if($order[status] == 2){ ?>
                <td>En espera. <a class="btn btn-primary" href="?p=view_order&id=<?php echo $order [id]; ?>">Ver</a></td>
            <?php } else if($order[status] == 3){ ?>
                <td>Finalizado.</td>
            <?php } else if($order[status] == 4){ ?>
                <td>Cancelado.</td>
            <?php } ?>
        </tr>

    <?php }} ?>
    </tbody>
</table>