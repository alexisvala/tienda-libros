<?php ?>
<h2><b>Clientes</b></h2>
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">Nombre</th>
        <th scope="col">Acciones</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $getUser = mysqli_query($conn, "SELECT * FROM `users`");
    if(mysqli_num_rows($getUser) != 0){
        while ($user = mysqli_fetch_array($getUser)) { ?>
            <tr>
                <th scope="row"><?php echo $user[firstname]." ". $user[lastname];?></th>
                <td colspan="1"> <a class="btn btn-primary" href="?p=all_orders&id=<?php echo $user[id]; ?>">Ver ordenes</a></td>
            </tr>

        <?php }} ?>
    </tbody>
</table>