<?php
include("core/search.php");
require_once("core/add_order.php");
$id = $_GET['id'];
if($id != NULL){
    $getBook = mysqli_query($conn, "SELECT * FROM `books` WHERE `id`='" . $id . "' LIMIT 1");
    while ($book = mysqli_fetch_array($getBook)) {
        $getCate = mysqli_query($conn, "SELECT * FROM `categories` WHERE `id`='" . $id . "' LIMIT 1");
        $cate = mysqli_fetch_array($getCate);
?>
        <div class="book_preview" style="background: url(<?php echo $book[image];?>)">
            <div class="image_book"></div>
            <h2><b><?php echo $book[name];?></b></h2>
            <h5><b>Autor:</b> <?php echo $book[autor];?></h5>
            <h5><b>ISBN:</b> <?php echo $book[isbn];?></h5>
            <h5><b>Editorial:</b> <?php echo $book[publisher];?></h5>
            <h5><b>Categoría:</b> <?php echo $cate[name];?></h5>

            <h5><b>Sinopsis:</b> <?php echo $book[synopsis];?></h5>
            <div style="float:right;">
                <h3><b>$<?php echo number_format($book[price], 2, '.', '');?></b></h3>
                <form action="#" method="post">
                    <button type="submit" class="btn btn-primary btn-lg" name="orderBook" value="<?php echo $book[id];?>">Comprar</button>
                </form>
            </div>
        </div>
        <div style="width: 100%;">
            <h2>Te pueden interesar...</h2><br>
            <div class="card-deck">
            <?php
            $getBooks = mysqli_query($conn,"SELECT * FROM `books` WHERE category = $book[category] AND id <> $book[id] ORDER BY id DESC LIMIT 4");
            while ($book = mysqli_fetch_array($getBooks)) {
                cardBook($book);
            } ?>
            </div>
        </div>

<?php } }?>

