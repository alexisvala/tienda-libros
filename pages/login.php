<?php
require_once("core/loginIn.php");
?>

<h2>Iniciar sesión</h2>
<p>Ingrese los datos necesarios para iniciar sesión.</p>
<form action="#" method="post">
    <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
        <label><b>Nombre de usuario</b></label>
        <input type="text" name="username" class="form-control" value="<?php echo $username; ?>">
        <span class="help-block"><?php echo $username_err; ?></span>
    </div>
    <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
        <label><b>Contraseña</b></label>
        <input type="password" name="password" class="form-control">
        <span class="help-block"><?php echo $password_err; ?></span>
    </div>
    <div class="form-group">
        <input type="submit" style="float:right;" class="btn btn-primary" value="Entrar">
    </div>
    <p>ó <a href="?p=register">Crear una cuenta.</a>.</p>
</form>
