<?php
require_once("core/complete_order.php");
?>
<script>
    $(document).ready(
        function() {
            $('select[name=envio]').change(
                function(){
                    var envio = $('option:selected',this).val();
                    var precio = $('input[name=basePrice]').val();
                    var costo = 0;
                    if(envio == "dhl"){
                        costo = 50;
                    }else if(envio == "fedex"){
                        costo = 55;
                    }else if(envio == "ups") {
                        costo = 60;
                    }
                    precio = parseFloat(precio) + costo;
                    $('#totalPrice').text('$' + precio);
                }
            );
        }
    );
</script>
<h2><b>Pedido</b></h2>
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Nombre</th>
        <th scope="col">Precio</th>
        <th scope="col">Acciones</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $getOrder = mysqli_query($conn, "SELECT * FROM `orders` WHERE `idUser`='" . $_SESSION["id"] . "' AND status = 1 LIMIT 1");
    if(mysqli_num_rows($getOrder) != 0){
        $orderInfo = mysqli_fetch_array($getOrder);

        $getBooks = mysqli_query($conn,"SELECT orders_books.*, books.* FROM orders_books LEFT JOIN books ON books.id = orders_books.idBook WHERE orders_books.idOrder = $orderInfo[id]");
        $i = 1;
        $total_price = 0;
        while ($book = mysqli_fetch_array($getBooks)) {?>
            <tr>
                <th scope="row"><?php echo $i; ?></th>
                <td><?php echo $book[name];?></td>
                <td><?php echo  number_format($book[price], 2, '.', '');?></td>
                <td>...</td>
            </tr>

    <?php  $total_price = $total_price + $book[price]; $i++;} } ?>
    </tbody>
</table>
<?php if(mysqli_num_rows($getOrder) != 0){ ?>
<form action="#" method="post">
    <div class="form-row">
        <div class="form-group col-md-6">
            <label><b>Método de pago</b></label>
            <select name="pago" class="form-control">
                <option value="tarjeta">Tarjeta de crédito</option>
                <option value="tarjeta">Cheque</option>
            </select>

        </div>

        <div class="form-group col-md-6">
            <label><b>Forma de envío</b></label>
            <select name="envio" class="form-control">
                <option>Seleccione...</option>
                <option value="dhl">DHL (+$50.00)</option>
                <option value="fedex">FEDEX (+$55.00)</option>
                <option value="ups">UPS (+$60.00)</option>
            </select>
        </div>

        <div class="form-group col-md-6">
            <label><b>Dirección</b></label>
            <input type="text" name="adress" class="form-control" id="exampleInputPassword1" placeholder="Dirección">
        </div>
    </div>
    <div style="float:right;">
        <h3><b id="totalPrice">$<?php echo number_format($total_price, 2, '.', '');?></b></h3>
        <form action="#" method="post">
            <input hidden name="basePrice" value="<?php echo number_format($total_price, 2, '.', '')?>">
            <button type="submit" class="btn btn-primary btn-lg" name="orderFinalBook" value="<?php echo $orderInfo[id];?>">Finalizar orden</button>

    </div>
</form>
<?php } ?>
