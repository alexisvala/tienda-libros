<?php require_once("core/cancel_order.php"); ?>
<h2><b>Pedidos</b></h2>
<table class="table table-striped">

    <?php
    $getOrder = mysqli_query($conn, "SELECT * FROM `orders` WHERE `idUser`='" . $_SESSION["id"] . "'");
    if(mysqli_num_rows($getOrder) != 0){
    while ($order = mysqli_fetch_array($getOrder)) { ?>
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Nombre</th>
        <th scope="col">Precio</th>
        <th scope="col">Estatus</th>
    </tr>
    </thead>
    <tbody>
        <tr>
        <th scope="row">Orden #<?php echo $order[id];?></th>
        <td colspan="1"></td>
            <td colspan="1">Total: <?php echo $order[finalPrice];?> </td>
            <?php if($order[status] == 1){ ?>
                <td>Editando. <br><a  class="btn btn-primary" href="?p=my_order">Completar orden</a></td>
            <?php }else if($order[status] == 2){ ?>
                <td>En espera.
                    <form action="#" method="post">
                            <button type="submit" class="btn btn-danger btn-lg" name="orderCancel" value="<?php echo $order[id];?>">Cancelar</button>
                    </form>
                </td>
            <?php } else if($order[status] == 3){ ?>
                <td>Finalizado.</td>
            <?php } else if($order[status] == 4){ ?>
                <td>Cancelado.</td>
            <?php } ?>
        </tr>
    <?php
        $getBooks = mysqli_query($conn,"SELECT orders_books.*, books.* FROM orders_books LEFT JOIN books ON books.id = orders_books.idBook WHERE orders_books.idOrder = $order[id]");
        $i = 1;
        $total_price = 0;
        while ($book = mysqli_fetch_array($getBooks)) {?>
            <tr>
                <th scope="row"><?php echo $i; ?></th>
                <td><?php echo $book[name];?></td>
                <td><?php echo number_format($book[price], 2, '.', '');?></td>
                <td>-</td>
            </tr>

    <?php  $total_price = $total_price + $book[price]; $i++;}
    echo "<br></tbody>";
    }} ?>
</table>