<?php
    require_once("core/registration.php");
?>
<h2>Registro</h2>
<p>Ingrese los datos necesarios para su registro.</p>
    <form action="#" method="post">
        <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
                <label><b>Nombre de usuario</b></label>
                <input type="text" name="username" class="form-control" value="<?php echo $username; ?>">
                <span class="help-block"><?php echo $username_err; ?></span>
        </div>
        <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
            <label><b>Nombre</b></label>
            <div class="form-row">
                <div class="col">
                    <input type="text" name="firstname" class="form-control"  placeholder="Nombres" <?php echo $firstname; ?>">
                </div>
                <div class="col">
                    <input type="text" name="lastname" class="form-control" placeholder="Apellidos"  value="<?php echo $lastname; ?>">
                </div>
            </div>
            <span class="help-block"><?php echo $name_err; ?></span>
        </div>

        <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
            <label><b>Contraseña</b></label>
            <input type="password" name="password" class="form-control mb-2" value="<?php echo $password; ?>">
            <span class="help-block"><?php echo $password_err; ?></span>
        </div>
        <div class="form-group <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">
            <label><b>Confirme Contraseña</b></label>
            <input type="password" name="confirm_password" class="form-control mb-2" value="<?php echo $confirm_password; ?>">
            <span class="help-block"><?php echo $confirm_password_err; ?></span>
        </div>
        <div class="form-group">
            <input type="reset" class="btn btn-default" value="Limpiar">
            <input style="float:right;" type="submit" class="btn btn-primary" value="Registro">
        </div>
        <p>ó <a href="?p=login">Iniciar sesión</a>.</p>
    </form>