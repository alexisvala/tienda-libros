<?php
require_once("core/finish_order.php");
require_once("core/cancel_order.php");
require_once("core/split_order.php");
?>
<h2><b>Pedido - Detalle</b></h2>
<form action="#" method="post">
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Nombre</th>
        <th scope="col">Precio</th>
        <th scope="col">Acciones</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $getOrder = mysqli_query($conn, "SELECT * FROM `orders` WHERE `id`='" . $_GET["id"] . "'LIMIT 1");
    if(mysqli_num_rows($getOrder) != 0){
        $orderInfo = mysqli_fetch_array($getOrder);

        $getBooks = mysqli_query($conn,"SELECT orders_books.*, books.* FROM orders_books LEFT JOIN books ON books.id = orders_books.idBook WHERE orders_books.idOrder = $orderInfo[id]");
        $i = 1;
        $total_price = 0;
        while ($book = mysqli_fetch_array($getBooks)) {?>
            <tr>
                <th scope="row"><?php echo $i; ?></th>
                <td><?php echo $book[name];?></td>
                <td><?php echo $book[price];?></td>
                <td><input type="checkbox" name="check_list[]" value="<?php echo $book[id];?>"></td>
            </tr>

    <?php  $total_price = $total_price + $book[price]; $i++;} } ?>
    </tbody>
</table>
<?php if(mysqli_num_rows($getOrder) != 0){ ?>
    <h3><b>Domicilio: </b></h3><h4><?php echo $orderInfo[adress];?></h4>
    <div style="float:right;">
        <h3><b id="totalPrice">$<?php echo number_format($orderInfo[finalPrice], 2, '.', '');?></b></h3>
        <button type="submit" class="btn btn-danger btn-lg" name="orderSplit" value="<?php echo $orderInfo[id];?>">Separar orden</button>
        <button type="submit" class="btn btn-danger btn-lg" name="orderCancel" value="<?php echo $orderInfo[id];?>">Cancelar orden</button>
        <button type="submit" class="btn btn-primary btn-lg" name="orderFinishBook" value="<?php echo $orderInfo[id];?>">Finalizar orden</button>

    </div>
</form>

<?php } ?>
