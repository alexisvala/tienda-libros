<?php
if($_SERVER["REQUEST_METHOD"] == "POST" && $_POST['orderCancel']) {
    $sql = "UPDATE orders SET status = ? WHERE id = ?";

    if ($stmt = mysqli_prepare($conn, $sql)) {
        mysqli_stmt_bind_param($stmt, "ii", $param_status,  $param_idOrder);

        $param_status = 4;
        $param_idOrder = $_POST["orderCancel"];

        if (mysqli_stmt_execute($stmt)) {
            header("location: /");
        } else {
            echo "Algo salió mal. Intente más tarde.";
        }
        exit;
    }
}
?>