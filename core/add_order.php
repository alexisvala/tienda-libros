<?php
if($_SERVER["REQUEST_METHOD"] == "POST") {
    $getOrder = mysqli_query($conn, "SELECT * FROM `orders` WHERE `idUser`='" . $_SESSION["id"] . "' AND status = 1 LIMIT 1");
    if (mysqli_num_rows($getOrder) == 0) {
        $sql = "INSERT INTO orders (idUser, status) VALUES (?, ?)";

        if ($stmt = mysqli_prepare($conn, $sql)) {
            mysqli_stmt_bind_param($stmt, "ii", $param_idUser, $param_status);

            $param_idUser = $_SESSION["id"];
            $param_status = 1;
            if (!mysqli_stmt_execute($stmt)) {
                echo "Algo salió mal. Intente más tarde.";
                exit;
            }
        }
    }

    $getOrder = mysqli_query($conn, "SELECT * FROM `orders` WHERE `idUser`='" . $_SESSION["id"] . "' AND status = 1 LIMIT 1");
    $orderInfo = mysqli_fetch_array($getOrder);
    $idBook = trim($_POST["orderBook"]);
    $sql = "INSERT INTO orders_books (idOrder, idBook, price) VALUES (?, ?, ?)";

    if ($stmt = mysqli_prepare($conn, $sql)) {
        mysqli_stmt_bind_param($stmt, "iid", $param_idOrder, $param_idBook, $param_price);

        $param_idOrder = $orderInfo[id];
        $param_idBook = $idBook;
        $getBook = mysqli_query($conn, "SELECT * FROM `books` WHERE `id`='" . $idBook . "' LIMIT 1");
        $book = mysqli_fetch_array($getBook);
        $param_price = $book[price];

        if (mysqli_stmt_execute($stmt)) {
            header("location: ?p=my_order");
        } else {
            echo "Algo salió mal. Intente más tarde.";
        }
        exit;
    }
}


