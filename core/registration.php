<?php

if(isLoggedIn()){
    header("location: /");
    exit;
}

$username = $password = $confirm_password = $firstname = $lastname = "";
$username_err = $password_err = $confirm_password_err = $name_err = "";

if($_SERVER["REQUEST_METHOD"] == "POST"){
    if(empty(trim($_POST["username"]))){
        $username_err = "Ingrese un nombre de usuario.";
    } else{
        $sql = "SELECT id FROM users WHERE username = ?";

        if($stmt = mysqli_prepare($conn, $sql)){
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            $param_username = trim($_POST["username"]);

            if(mysqli_stmt_execute($stmt)){
                mysqli_stmt_store_result($stmt);

                if(mysqli_stmt_num_rows($stmt) == 1){
                $username_err = "Este usuario ya ha sido escogido. Intente con otro.";
                } else{
                $username = trim($_POST["username"]);
                }
            } else{
                echo "Algo salió mal. Intente más tarde.";
            }
        }
        mysqli_stmt_close($stmt);
    }

    if(empty(trim($_POST["password"]))){
        $password_err = "Ingrese una contraseña.";
    } elseif(strlen(trim($_POST["password"])) < 6){
        $password_err = "Contraseña debe ser mayor a 6 caracteres.";
    } else{
        $password = trim($_POST["password"]);
    }

    if(empty(trim($_POST["confirm_password"]))){
        $confirm_password_err = "Confirme contraseña.";
    } else{
        $confirm_password = trim($_POST["confirm_password"]);
        if(empty($password_err) && ($password != $confirm_password)){
            $confirm_password_err = "Contraseñas no coinciden.";
        }
    }

    if(empty(trim($_POST["firstname"])) || empty(trim($_POST["lastname"]))){
        $name_err = "Ingrese su nombre.";
    } else{
        $firstname = trim($_POST["firstname"]);
        $lastname = trim($_POST["lastname"]);
    }

    if(empty($username_err) && empty($password_err) && empty($confirm_password_err) && empty($name_err)){

        $sql = "INSERT INTO users (username, password, firstname, lastname) VALUES (?, ?, ?, ?)";

        if($stmt = mysqli_prepare($conn, $sql)){
            mysqli_stmt_bind_param($stmt, "ssss", $param_username, $param_password,  $param_firstname, $param_lastname);

            $param_username = $username;
            $param_password = password_hash($password, PASSWORD_DEFAULT);
            $param_firstname = $firstname;
            $param_lastname = $lastname;

            if(mysqli_stmt_execute($stmt)){
                header("location: ?p=login");
            } else{
                echo "Algo salió mal. Intente más tarde.";
            }
        }
        mysqli_stmt_close($stmt);
    }
    mysqli_close($conn);
}
?>