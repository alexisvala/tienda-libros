<?php
if($_SERVER["REQUEST_METHOD"] == "POST" && $_POST['orderSplit']) {
    if(isset($_POST['check_list'])) {
        $sql = "INSERT INTO orders (idUser, status, shipping, finalPrice, adress) SELECT idUser,status,shipping, finalPrice,adress FROM orders WHERE id= ?";

        if ($stmt = mysqli_prepare($conn, $sql)) {
            mysqli_stmt_bind_param($stmt, "i", $param_id);

            $param_id =  $_POST['orderSplit'];

            if (!mysqli_stmt_execute($stmt)) {
                echo "Algo salió mal. Intente más tarde.";
                exit;
            }
        }
        $newId = mysqli_insert_id($conn);

        $lessprice = 0;
        $values = $_POST['check_list'];

        foreach($values as $selected){
            $sql = "UPDATE orders_books SET idOrder = ? WHERE id = ?";
            if ($stmt = mysqli_prepare($conn, $sql)) {
                mysqli_stmt_bind_param($stmt, "ii", $param_idOrder,  $param_idBook);

                $param_idOrder = $newId;
                $param_idBook = $selected;

                if (mysqli_stmt_execute($stmt)) {
                    $getBook = mysqli_query($conn, "SELECT * FROM `orders_books` WHERE `id`= " . $selected. " LIMIT 1");
                    $bookInfo = mysqli_fetch_array($getBook);
                    $lessprice =  $lessprice + $bookInfo[price];
                } else {
                    echo "Algo salió mal. Intente más tarde.";
                }
            }
        }

        $sql = "UPDATE orders SET finalPrice = finalprice - ? WHERE id = ?";

        if ($stmt = mysqli_prepare($conn, $sql)) {
            mysqli_stmt_bind_param($stmt, "ii", $param_lessprice,  $param_idOrder);

            $param_lessprice =  $lessprice;
            $param_idOrder= $_POST['orderSplit'];

            if (!mysqli_stmt_execute($stmt)) {
                echo "Algo salió mal. Intente más tarde.";
            }
        }

        $sql = "UPDATE orders SET finalprice = ? WHERE id = ?";

        if ($stmt = mysqli_prepare($conn, $sql)) {
            mysqli_stmt_bind_param($stmt, "ii", $param_lessprice,  $param_idOrder);

            $param_lessprice =  $lessprice;
            $param_idOrder= $newId;

            if (mysqli_stmt_execute($stmt)) {
                header("location: ?p=all_orders");
            } else {
                echo "Algo salió mal. Intente más tarde.";
            }
            exit;
        }
    }
}
?>