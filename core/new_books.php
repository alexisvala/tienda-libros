<?php
if(!isLoggedIn() || !$isAdmin){
    header("location: /");
    exit;
}
$name = $isbn = $autor = $publisher = $category = $image = $price = $synopsis = $stock = "";
$name_err = $isbn_err = $autor_err = $publisher_err = $category_err = $image_err = $price_err = $synopsis_err = $stock_err = "";

if($_SERVER["REQUEST_METHOD"] == "POST"){
    if(empty(trim($_POST["name"]))){
        $name_err = "Ingrese un nombre de libro.";
    } else{
        $name = trim($_POST["name"]);
    }
    if(empty(trim($_POST["isbn"]))){
        $isbn_err = "Ingrese ISBN.";
    } else{
        $isbn = trim($_POST["isbn"]);
    }
    if(empty(trim($_POST["autor"]))){
        $autor_err = "Ingrese autor.";
    } else{
        $autor = trim($_POST["autor"]);
    }
    if(empty(trim($_POST["publisher"]))){
        $publisher_err = "Ingrese editorial.";
    } else{
        $publisher = trim($_POST["publisher"]);
    }
    if(empty(trim($_POST["category"]))){
        $category_err = "Ingrese categoría.";
    } else{
        $category = trim($_POST["category"]);
    }

    $image = trim($_POST["image"]);

    if(empty(trim($_POST["price"]))){
        $price_err = "Ingrese precio.";
    } else{
        $price = trim($_POST["price"]);
    }

    $synopsis = trim($_POST["synopsis"]);
    $stock = trim($_POST["stock"]);


    if(empty($name_err) && empty($isbn_err) && empty($autor_err) && empty($publisher_err) && empty($category_err) && empty($image_err) && empty($price_err) && empty($synopsis_err) && empty($stock_err)){

        $sql = "INSERT INTO books (name,isbn,autor,publisher,category,image,price,synopsis,stock,available) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?,?)";

        if($stmt = mysqli_prepare($conn, $sql)){
            mysqli_stmt_bind_param($stmt, "ssssisdsii", $param_name ,$param_isbn , $param_autor , $param_publisher , $param_category , $param_image , $param_price , $param_synopsis , $param_stock, $param_avaiable);

            $param_name = $name;
            $param_isbn = $isbn;
            $param_autor = $autor;
            $param_publisher = $publisher;
            $param_category =$category;
            $param_image  = $image;
            $param_price = $price;
            $param_synopsis =$synopsis;
            $param_stock = $stock;
            $param_avaiable = 1;

            if(mysqli_stmt_execute($stmt)){
                header("location: /");
            } else{
                echo "Algo salió mal. Intente más tarde.";
            }
        }
        mysqli_stmt_close($stmt);
    }
    mysqli_close($conn);
}
?>