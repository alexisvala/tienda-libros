<?php
if($_SERVER["REQUEST_METHOD"] == "POST") {
    $getOrder = mysqli_query($conn, "SELECT * FROM `orders` WHERE `id`='" . $_POST["orderFinalBook"]. "' AND status = 1 LIMIT 1");
    $orderInfo = mysqli_fetch_array($getOrder);
    $idBook = trim($_POST["orderFinalBook"]);
    $sql = "UPDATE orders SET status = ?, shipping = ?,  finalPrice = ?, adress = ? WHERE id = ?";

    if ($stmt = mysqli_prepare($conn, $sql)) {
        mysqli_stmt_bind_param($stmt, "isdsi", $param_status, $param_shipping, $param_finalPrice, $param_adress, $param_idOrder);

        $param_status = 2;
        $param_shipping = trim($_POST["envio"]);
        $param_finalPrice = 0;
        $getBooks = mysqli_query($conn,"SELECT orders_books.*, books.* FROM orders_books LEFT JOIN books ON books.id = orders_books.idBook AND orders_books.idOrder = $orderInfo[id]");

        while ($book = mysqli_fetch_array($getBooks)) {
            $param_finalPrice = $param_finalPrice + $book[price];
        }

        if(trim($_POST["envio"]) == "dhl"){
            $param_finalPrice = $param_finalPrice + 50;
        }else if(trim($_POST["envio"]) == "fedex"){
            $param_finalPrice = $param_finalPrice + 55;
        }else if(trim($_POST["envio"]) == "ups"){
            $param_finalPrice = $param_finalPrice + 60;
        }else{
            exit;
        }
        $param_adress = trim($_POST["adress"]);
        $param_idOrder = $orderInfo[id];


        if (mysqli_stmt_execute($stmt)) {
            header("location: ?p=my_order");
        } else {
            echo "Algo salió mal. Intente más tarde.";
        }
        exit;
    }
}