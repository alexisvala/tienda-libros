<?php

function init(){
     if(!isset($_SESSION)) {
         session_start();
     }
}

function isLoggedIn()
{
    if (isset($_SESSION["loggedIn"])) {
        if ($_SESSION["loggedIn"])
            return true;
    }
    return false;
}

function logout(){
    $_SESSION = array();
    session_destroy();
    header("location: /");
    exit;
}

function cardBook($book){
    echo '<div class="card">
            <div class="card-img-top thumbnail_book"></div>
                <div class="card-body">
                    <h5 class="card-title text-truncate">'.$book[name]. '</h5>
                    <p class="card-text">'.$book[autor].'</p>
                </div>
            <div class="card-footer">
                <div style="float:left;"> <span class="badge badge-pill badge-info">Nuevo</span><br> <span class="font-weight-bold">$'.number_format($book[price], 2, '.', '').'</span></span></div>
                <a  style="float:right;" class="btn btn-primary" href="?p=book&id='.$book[id].'">Comprar</a>
            </div>
        </div>';
}
init();
?>