<?php
if($_SERVER["REQUEST_METHOD"] == "POST") {
    $url = "?p=search&search=".$_POST["search"]."&by=".$_POST["search_by"];
    header("location:" . $url );
}
?>
<form action="#" class="form-inline mt-2 mt-md-0" method="post">
    <div id="search" class="form-group row" >
        <label id="welcome_text" for="colFormLabelLg" class="col-form-label col-form-label-lg"></label>
        <div class="">
            <input type="search" class="form-control form-control-lg " id="search_input" name="search" placeholder="Encuentra tu libro">
        </div>
        <select name="search_by" class="custom-select my-1 mr-sm-2" id="search_by">
            <option value="1" selected>Nombre</option>
            <option value="2">Autor</option>
            <option value="3">ISBN</option>
        </select>
        <button class="btn btn-outline-primary my-2 my-sm-0 col-form-label-lg" type="submit">Buscar</button>
    </div>
</form>